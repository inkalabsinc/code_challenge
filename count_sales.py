import sys
import csv
import os
import shutil

from os import listdir


def main():
    """
    The algorithm used for the problem proposed of having a cvs file which
    content is composed by departments, date and its sales count, does the
    following:
    First we start reading the origin file line by line, for each
    line, the algorithm tries to open a file with the reading department name,
    loads the existing count and adds the reading count then writes the new
    total. If the department file does not exist it creates it with the reading
    count, once the origin file has been read completely it
    creates the result file and reads file by file of the resulting temporary folder
    copying the results into the results file

    This way the memory will not be fulled in any case because we use I/O in
    storage memory to avoid the use of ram memory.
    Eficiency for this algorithm would be = 2(n)(IO) where IO is a time constant
    of writing and reading on disk
    Notes: To improve the eficiency on this algorithm we considered using panda
    library to open the original file in chunks depending on the memory size,
    but the simplitcity was lost, therefor I declined to use this approach.
    """

    file_path = input('File path: ')
    tmp_folder = 'tmp'
    result_file = 'results.csv'
    if not os.path.exists(tmp_folder):
        os.makedirs(tmp_folder)
    for line in open(file_path, encoding='utf-16'):
        department, _, count = line.strip(' \n\t').split(',')
        department_file_name = f"{tmp_folder}/{department}.csv"
        if not os.path.exists(department_file_name): # opens the temporary csv apartment file
            with open(department_file_name, 'w', newline='') as tmp_file:
                writer = csv.writer(tmp_file, delimiter=',')
                writer.writerow([count])
        else: # creates the temporary csv apartment file
            with open(department_file_name, 'r+') as tmp_file:
                tmp_count = int(tmp_file.read())
                tmp_count += int(count)
                tmp_file.seek(0)
                tmp_file.write(str(tmp_count))

    if not os.path.exists(result_file):
        f = open(result_file, 'x')
    with open(result_file, 'w') as result_file:
        for csv_file in [f for f in listdir(f"{os.getcwd()}/{tmp_folder}")]: # loops through the temporary folder content
            with open(f"{tmp_folder}/{csv_file}") as tmp_file:
                tmp_count = int(tmp_file.read())
                result_file.write(f"{csv_file.split('.')[0]},{str(tmp_count)}\n")
    shutil.rmtree(tmp_folder) # removes the temporary folder and its content


if __name__ == '__main__':
    main()
